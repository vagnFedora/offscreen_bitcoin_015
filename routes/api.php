<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'keypoll'], function() {
    Route::post('fee', 'TransactionController@fee');
    Route::post('operation', 'OperationController@index');
    Route::get('notify/{txid}', 'TransactionController@notify');

    if (env('APP_ENV') === 'local') {
        Route::post('send', 'TransactionController@send');
        Route::post('newaddress', 'AddressController@create');
        Route::post('increment', 'BalanceController@increment');
        Route::post('decrement', 'BalanceController@decrement');
    }
    Route::get('balance/{address}', 'BalanceController@show');
});
    Route::post('syncwallet', 'AddressController@syncwallet')->middleware('throttle:10000,1');

