<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Enum\OperationTypeEnum;
use App\Enum\VersionCoreEnum;

class TransactionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    public function send(Request $request) {
        return self::create($request->all());
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public static function create($data) {
        try {
            if (env('BALANCE_CHECK')) {
                BalanceController::check($data['fromAddress'], $data['balance']);
            }
            $authenticate = self::_checkAuthenticity($data);

            $amount = 0;
            $total = ($data['amount'] + $data['fee']);

            $output = bitcoind()->listunspent();
            $result = $output->get();

            $translist = [];

            foreach ($result as $key => $saida) {
                $translist[] = [
                    'txid' => $saida['txid'],
                    'vout' => $saida['vout'],
                    'scriptPubKey' => $saida['scriptPubKey'],
                    'redeemScript' => $authenticate['redeemScript'],
                    'amount' => $saida['amount']
                ];

                $amount += $saida['amount'];
                if ($amount >= $total) {
                    break;
                }
            }

            $rawchangeaddress = (bitcoind()->getrawchangeaddress())->get();
            $rest = sprintf('%.8f', $amount) - sprintf('%.8f', $total);
            $where[$data['toAddress']] = sprintf('%.8f', $data['amount']);
            $where[$rawchangeaddress] = sprintf('%.8f', $rest);
            $hex = bitcoind()->createrawtransaction($translist, $where);
            bitcoind()->walletpassphrase($authenticate['key'], 5);
            $sender = bitcoind()->sendrawtransaction($hex->get());
            bitcoind()->walletlock();
            return $sender->get();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     *
     * @param type $transaction
     * @return type
     * @throws \Exception
     */
    private static function _checkAuthenticity($transaction) {
        try {

            $response = GuzzleController::postOffscreen(OperationTypeEnum::CHECK_AUTHENTICITY, $transaction);
            if (!$response) {
                throw new \Exception("[ECI]", 422);
            }
            return self::_getKeys();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     *
     * @return type
     * @throws \Exception
     */
    private static function _getKeys() {
        $response = GuzzleController::postSign();
        if (!$response) {
            throw new \Exception("[KSI]");
        }
        return $response;
    }

    /**
     *
     * Assina as transações
     *
     * @param type $hex
     * @param type $unspend
     * @param type $privKey
     * @return type
     */
    private static function signrawtransaction($hex, $unspend, $privKey) {
        return (bitcoind()->signrawtransaction($hex, $unspend, $privKey))->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($txid) {
        //
    }

    /**
     *
     * @return type
     * @throws \Exception
     */
    public function keys() {
        $response = GuzzleController::postSign();
        if (!$response) {
            throw new \Exception("[KSI]");
        }
        return $response;
    }

    /**
     *
     * @param type $txid
     * @return type
     */
    public function notify($txid) {
        try {
            $data = $this->_gettransaction($txid);
            if ($data) {
                foreach ($data as $value) {
                    GuzzleController::postOffscreen(OperationTypeEnum::NOTIFY_WALLET, $value);
                }
            }
        } catch (\Exception $ex) {
            
        }
    }

    /**
     *
     * @param type $txid
     * @return type
     */
    public function confirmation($txid) {
        $gettransaction = bitcoind()->gettransaction($txid);
        return $gettransaction->get();
    }

    /**
     *
     * @param type $txid
     * @return type
     */
    private function _gettransaction($txid) {
        $gettransaction = bitcoind()->gettransaction($txid);
        $transactionData = $gettransaction->get();
        $data = [];
        foreach ($transactionData['details'] as $transaction) {

            $data[] = [
                'amount' => abs($transaction['amount']),
                'fee' => isset($transactionData['fee']) ? abs($transactionData['fee']) : 0,
                'confirmations' => $transactionData['confirmations'],
                'txid' => $transactionData['txid'],
                'toAddress' => $transaction['address']
            ];
        }
        return $data;
    }

    /**
     * 
     * @param type $conf_target
     */
    public static function estimateFee($conf_target) {

        switch (env("VERSION_CORE")) {
            case VersionCoreEnum::V015:
                $gettransaction = bitcoind()->estimatefee($conf_target);
                $result = $gettransaction->get();
                break;

            case VersionCoreEnum::V016:
                $gettransaction = (bitcoind()->estimatesmartfee($conf_target))->get();
                $result = $gettransaction['feerate'];
                break;

            default:
                $result = 0;
                break;
        }

        return $result;
    }

    public static function received() {
        $gettransactions = bitcoind()->listreceivedbyaddress();
        $result = $gettransactions->get();
        return $result;
    }

    public function fee() {
        return self::estimateFee(6);
    }

}
