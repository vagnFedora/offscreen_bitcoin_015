<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller {

    /**
     * Create new address.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create() {
        try {

            $bitcoind = bitcoind()->getNewAddress();
            $address = json_decode($bitcoind->getBody());
            $operationController = new OperationController();

            Address::create([
                'wallet' => $address->result,
                'balance' => $operationController->_encryptResponse('0.00000000')
            ]);

            return $address->result;
        } catch (\Exception $ex) {

            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Display info.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public static function show($address) {
        try {

            $bitcoind = bitcoind()->getAddressInfo($address);
            $address = json_decode($bitcoind->getBody());
            return $address->result;
        } catch (\Exception $ex) {

            throw new \Exception($ex->getMessage());
        }
    }

    
    public function syncwallet(Request $request){
        
        if(env("APP_PASS") !== $request->key){
            throw new \Exception("Invalid Key");
        }
        
        return self::sync2app($request->all());
    }
            
    
    public static function sync2app($data) {
        $operationController = new OperationController();
        $address = Address::where('wallet', $data['address'])->first();
        $address->balance = $operationController->_encryptResponse($data['amount']);
        $address->save();
    }

}
